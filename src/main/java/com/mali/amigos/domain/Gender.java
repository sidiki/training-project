package com.mali.amigos.domain;

public enum Gender {
    MALE,
    FEMALE,
    OTHER
}
