package com.mali.amigos.service;


import lombok.AllArgsConstructor;
import java.util.List;
import org.springframework.stereotype.Service;
import com.mali.amigos.domain.Student;
import com.mali.amigos.domain.StudentRepository;
import com.mali.amigos.exception.BadRequestException;
import com.mali.amigos.exception.StudentNotFoundException;

@Service
@AllArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;

    public List<Student> getAllStudents() {
        return studentRepository.findAll();
    }

    public void addStudent(Student student) {
        Boolean existsEmail = studentRepository.selectExistsEmail(student.getEmail());

        if (existsEmail) {
            throw new BadRequestException("Email " + student.getEmail() + " taken");
        }

        studentRepository.save(student);
    }

    public void deleteStudent(Long studentId) {
        if(!studentRepository.existsById(studentId)) {
            throw new StudentNotFoundException("Student with id " + studentId + " does not exists");
        }
        studentRepository.deleteById(studentId);
    }
}
