package com.mali.amigos.student;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import com.mali.amigos.domain.Gender;
import com.mali.amigos.domain.Student;
import com.mali.amigos.domain.StudentRepository;
import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class StudentRepositoryTest {

    @Autowired
    private StudentRepository underTest;

    @AfterEach
    void tearDown() {
        underTest.deleteAll();
    }

    @Test
    void itShouldCheckIfStudentEmailExists() {
        // given
        var email = "aicha@email.com";

        var student = new Student("Aicha", email, Gender.FEMALE);
        underTest.save(student);

        // when
        var expected = underTest.selectExistsEmail(email);

        // then
        assertThat(expected).isTrue();
    }

    @Test
    void itShouldCheckIfStudentEmailNotExists() {
        // given
        var email = "aicha@email.com";

        var student = new Student("Aicha", email, Gender.FEMALE);

        // when
        var expected = underTest.selectExistsEmail(email);

        // then
        assertThat(expected).isFalse();
    }
}
