package com.mali.amigos.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import com.mali.amigos.domain.Gender;
import com.mali.amigos.domain.Student;
import com.mali.amigos.domain.StudentRepository;
import com.mali.amigos.exception.BadRequestException;
import com.mali.amigos.exception.StudentNotFoundException;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class StudentServiceTest {

    @Mock private StudentRepository studentRepository;
    //private AutoCloseable autoCloseable;
    private StudentService underTest;

    @BeforeEach
    void setUp() {
        //autoCloseable = MockitoAnnotations.openMocks(this);
        underTest = new StudentService(studentRepository);
    }

    /*

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

     */

    @Test
    void getAllStudents() {

        // when
        underTest.getAllStudents();

        // then
        verify(studentRepository).findAll();
    }

    @Test
    void shouldAddNewStudent() {
        // given
        var student = new Student("Aicha", "email@email.com", Gender.FEMALE);

        // when
        underTest.addStudent(student);

        // then
        ArgumentCaptor<Student> studentArgumentCaptor = ArgumentCaptor.forClass(Student.class);

        verify(studentRepository).save(studentArgumentCaptor.capture());

        var studentCaptured = studentArgumentCaptor.getValue();

        assertThat(studentCaptured).isEqualTo(student);
    }

    @Test
    void shouldThrowEmailTaken() {
        // given
        var student = new Student("Aicha", "email@email.com", Gender.FEMALE);

        // when
        given(studentRepository.selectExistsEmail(student.getEmail())).willReturn(true);

        // then
        assertThatThrownBy(() -> underTest.addStudent(student))
                .isExactlyInstanceOf(BadRequestException.class)
                .hasMessageContaining("Email " + student.getEmail() + " taken");

        verify(studentRepository, never()).save(any());
    }

    @Test
    void shouldDeleteStudent() {
        // given
        var studentId = 2L;
        given(studentRepository.existsById(studentId)).willReturn(true);

        // when
        underTest.deleteStudent(studentId);

        // then
        ArgumentCaptor<Long> studentIdCaptor = ArgumentCaptor.forClass(Long.class);

        verify(studentRepository).deleteById(studentIdCaptor.capture());

        var capturedId = studentIdCaptor.getValue();

        assertThat(capturedId).isEqualTo(studentId);
    }

    @Test
    void shouldThrowStudentNotFound() {
        // given
        var studentId = 2L;
        given(studentRepository.existsById(studentId)).willReturn(false);

        // when
        given(studentRepository.existsById(studentId)).willReturn(false);

        // then
        assertThatThrownBy(() -> underTest.deleteStudent(studentId))
                .isExactlyInstanceOf(StudentNotFoundException.class)
                .hasMessageContaining("Student with id " + studentId + " does not exists");

        verify(studentRepository, never()).deleteById(any());
    }
}
